$(document).ready(function(){

	var partialPanorama = document.getElementById('partial-panorama');
	
	if (typeof(partialPanorama) != 'undefined' && partialPanorama != null) {

		var PSVpartial = new PhotoSphereViewer({
			panorama: '../img/cylindrical.jpg',
			container: partialPanorama,
			time_anim: 3000,
			tilt_up_max: '0deg',
			tilt_down_max: '0deg',
			loading_msg: 'Loading ...',
			navbar: true,
			zoom_level: 60,
			navbar_style: {
				backgroundColor: 'rgba(0,0,0,.5)'
			},
		});

	}

	var fullPanorama = document.getElementById('full-panorama');

	if (typeof(fullPanorama) != 'undefined' && fullPanorama != null) {
		var PSVfull = new PhotoSphereViewer({
			panorama: '../img/equirectangular.jpg',
			// panorama: 'http://i.imgur.com/mHL803Q.jpg',
			container: fullPanorama,
			time_anim: 3000,
			loading_msg: 'Loading ...',
			navbar: true,
			zoom_level: 60,
			navbar_style: {
				backgroundColor: 'rgba(58, 67, 77, 0.7)'
			},
		});
	}
	
});