Panorama Viewer Exploration
===

Exploring different javascript and web based viewers for 360 deg equirectangular images.

Currently exploring Photo Sphere Viewer. Like there initialization options and API.

Libraries To Explore
===

[Peachananr: Simple left and right panning](https://github.com/peachananr/panorama_viewer)
[Marzipano: A viewer by google with a robust API](http://www.marzipano.net/)
[Pannellum: Popular viewer on github](https://pannellum.org/documentation/overview/)
[Photo Sphere Viewer: Nice viewer with a good API and options](https://github.com/JeremyHeleine/Photo-Sphere-Viewer)



